import Dimension from '../../utils/dimensionUtils';
import { baseColor } from '../../configs/configApp';
import { scale } from '../../utils/scalingUtils';

export default {
    container: {
        // height: Dimension.getScreenHeight() / 12,
        flex: 1,
        backgroundColor: baseColor,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: scale(17)
    }
}