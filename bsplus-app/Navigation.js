import React from 'react';
import { Icon } from 'react-native-elements'
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Contact from './src/screens/Contact';
import NewFeed from './src/screens/NewFeed'

export default createAppContainer(createBottomTabNavigator(
    {
        Chat: Contact,
        Contact: Contact,
        NewFeed: NewFeed,
        Payment: Contact
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let IconComponent = Icon;
                let iconName;
                if (routeName === 'Contact') {
                    iconName = `contacts${focused ? '' : ''}`;
                } else if (routeName === 'NewFeed') {
                    iconName = `book${focused ? '' : ''}`;
                } else if (routeName === 'Chat') {
                    iconName = `chat${focused ? '' : ''}`;
                } else {
                    iconName = `payment${focused ? '' : ''}`;
                }
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
        },
    }
))