/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Header from './src/components/header';
import AppContainer from './Navigation';

class App extends React.Component {

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 2 }}>
          <Header />
        </View>
        <View style={{ flex: 8 }}>
          <AppContainer />
        </View>
      </SafeAreaView>
    )
  }
};

export default App;
